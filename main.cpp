#include "widget.h"

#include <QApplication>
#include <QFile>
#include <QFont>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile qss(":/style.qss");
    qss.open(QFile::ReadOnly);
    qApp->setStyleSheet(qss.readAll());
    qss.close();

    QFont font("Microsoft YaHei", 12);
    a.setFont(font);
    Widget w;
    w.show();
    return a.exec();
}
